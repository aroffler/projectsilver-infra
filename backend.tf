
terraform {
  backend "s3" {
    bucket = "projectsilver-terraform-state"
    key = "tfstate"
    region = "us-east-1"
    profile = "tfuser"
  }
}

