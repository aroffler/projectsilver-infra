##---webserverstatic/outputs.tf---##
output "static_site_bucketname" {
  value = "${aws_s3_bucket.s3_site_bucket.id}"
}
