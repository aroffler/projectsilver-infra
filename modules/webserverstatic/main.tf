##---webserverstatic/main.tf---##

#Create s3 Bucket with static website settings
resource "aws_s3_bucket" "s3_site_bucket" {
  bucket = "${var.s3_site_name}"
  acl = "public-read"
  policy = "${file("./policies/staticwebsite_bucketpolicy.json")}"

  website {
    index_document = "index.html"
    error_document = "404.html" #name as 404 instead of error.html to match HUGO format for easier deployment
  }
  tags = {
    Name = "s3_Static_Website"
    CreatedBy = "aroffler"
  }
}
