
provider "aws" {
  version = "~> 2.12"
  region = "${var.region}"
  profile = "${var.profile}"
}
