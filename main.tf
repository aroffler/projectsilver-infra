
#Deploy the s3 static hosting bucket
module "webserverstatic" {
  source = "./modules/webserverstatic"
  project_name = "${var.project_name}"
}

