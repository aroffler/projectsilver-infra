variable "region" {default = "us-east-1"}
variable "s3_site_name" {default = "www.projectsilver.net"}
variable "profile" {default = "tfuser"}
variable "shared_creds_profile" {default = "/Users/aroffler/.aws/credentials"}
variable "project_name" {default = "projectsilver"}

